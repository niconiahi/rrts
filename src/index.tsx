import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

// components
import App from './components/App'

// reducers
import { reducers } from './store/reducers'

const store = createStore(reducers, applyMiddleware(thunk))

const rootElement = document.querySelector("#root")

const rootComponent = (
    <Provider store={store}>
        <App />
    </Provider>
)

ReactDOM.render(rootComponent, rootElement)