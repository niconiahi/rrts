import { combineReducers } from 'redux'

// reducers
import { todosReducer } from './todos'

// types
import { Todo } from '../actions/types'

// verifica que el `todosReducer` devuelva un array `Todo[]`
export interface StoreState {
    todos: Todo[]
}

export const reducers = combineReducers<StoreState>({
    todos: todosReducer
})