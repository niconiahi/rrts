import axios from 'axios'

// redux
import { Dispatch } from 'redux'

// types
import { ActionTypes, FetchTodosAction, Todo, DeleteTodoAction } from './types'

const url = 'https://jsonplaceholder.typicode.com/todos'

export const fetchTodos = () => {
    console.log('entro')
    return async (dispatch: Dispatch) => {
        const response = await axios.get<Todo[]>(url)

        dispatch<FetchTodosAction>({
            type: ActionTypes.fetchTodos,
            payload: response.data
        })
    }
}

export const deleteTodo = (id: number): DeleteTodoAction => {
    return {
        type: ActionTypes.deleteTodo,
        payload: id
    }
}