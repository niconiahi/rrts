export enum ActionTypes {
    fetchTodos,
    deleteTodo
}

export type Action = FetchTodosAction | DeleteTodoAction

// SEPARACION

// interface que describe esctructura de un todo
export interface Todo {
    id: number
    title: string
    completed: boolean
}

// typing pasandole una interface al generic de dispatch
export interface FetchTodosAction {
    type: ActionTypes.fetchTodos,
    payload: Todo[]
}

export interface DeleteTodoAction {
    type: ActionTypes.deleteTodo
    payload: number
}