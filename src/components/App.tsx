import React, { useCallback, useEffect } from 'react'

// redux
import { useSelector, useDispatch } from 'react-redux'

// types
import { ActionTypes } from '../store/actions/types'

// actions
// import { fetchTodos } from '../store/actions'

// reducers
import { StoreState } from '../store/reducers'

const App = () => {
    const dispatch = useDispatch()
    const todos = useSelector((state: StoreState) => state.todos)

    const fetchTodos = useCallback(
        () => dispatch({ type: ActionTypes.fetchTodos }),
        [dispatch]
    )

    const deleteTodo = useCallback(
        (id: number) => dispatch({ type: ActionTypes.deleteTodo, id }),
        [dispatch]
    )

    useEffect(() => {
        console.log('todos', todos)
    }, [todos])

    useEffect(() => {
        fetchTodos()
    }, [fetchTodos])

    return (
        <span>
            <ul>
                {todos.map(todo => <li key={todo.title} onClick={() => deleteTodo(todo.id)}>{todo.title}</li>)}
            </ul>
        </span>
    )
}

export default App