// import { fetchTodos } from "../store/actions";

// types
import { Todo, } from '../store/actions/types'

// actions 
import { deleteTodo } from '../store/actions'

export interface AppProps {
    todos: Todo[]
    fetchTodos: Function // porque estamos usando redux-thunk sino seria typeof fetchTodos 
    deleteTodo: typeof deleteTodo
}